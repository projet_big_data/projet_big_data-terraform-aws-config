# projet_big_data-terraform-aws-config

Script Terraform de déploiement de l'infrastructure AWS pour le projet Big Data (S3 Buckets, IAM, Sagemaker)

## Sources

- [Télécharger Terraform CLI](https://www.terraform.io/downloads.html)
- [Documentation Terraform AWS](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources)

## Commandes

`cd ./terraform` : déplacement dans le dossier terraform

`terraform init` : initialisation (nécessaire à chaque changement de _backend.tf_)

`terraform validate` : vérification de la syntaxe

`terraform fmt` : (optionnel) formatage enjolivé des fichiers _.tf_

`terraform plan` : plannification du déploiement (nécessaire avant déploiement)

`terraform apply` : déploiement sur AWS

`terraform desttroy` : suppression des ressources sur AWS

## Pré-requis 

Les scripts ne sont pas variabilisés, il est donc nécessaire de vérifier dans chaque fichier si les paramètres rentrés correspondent bien aux votres.

- bucket pour le .tfstate (_backend.tf_) à instancié directement avec AWS Console
- credentials AWS (_.aws_ dans votre dossier utilisateur)
- chemin d'accès au fichier _data.json_ (_s3.tf_)

## Automatisation

L'automatisation de l'exécution du notebook de Sagemaker n'est pas encore en place. Elle est prévue par le biais des applications AWS Lambda et Cloudwatch.

En attendant un noyau acceptant la version python3.8, il est aussi nécessaire de choisir le noyau `conda-python3`.

- [Explication de principe pour l'automatisation de Sagemaker](https://data-sleek.com/automating-aws-sagemaker-notebooks/)
- [Source pour la configration de Lambda avec Terraform](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/lambda_function)

## Démonstration

- [Vidéo de démonstration d'exécution des scripts Terraform pour le déploiement des solutions AWS](https://youtu.be/yXeOEhjCSGA)