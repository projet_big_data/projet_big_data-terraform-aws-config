terraform {
  backend "s3" {
    bucket = "myawsbucket-kbr"
    key    = "bigdata_cv_extract_classifier_sagemaker.tfstate"
    region = "us-east-1"
  }
}
provider "aws" {
  region = "us-east-1"
}

