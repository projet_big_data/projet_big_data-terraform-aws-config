resource "aws_sagemaker_code_repository" "projet_big_data" {
  code_repository_name = "projet-big-data-sagemaker-ni-git"
  git_config {
    repository_url = "https://gitlab.com/projet_big_data/projet_big_data-sagemaker_scripts.git"
  }
}

resource "aws_sagemaker_notebook_instance" "bigdata_sagemaker_ni" {
  name                    = "projet-big-data-sagemaker-ni"
  role_arn                = aws_iam_role.s3_allowed_role.arn
  instance_type           = "ml.t2.medium"
  default_code_repository = aws_sagemaker_code_repository.projet_big_data.code_repository_name

  tags = {
    Name = "bigdata"
  }
}
