resource "aws_kms_key" "mykey" {
  description             = "This key is used to encrypt bucket objects (for read files)"
  deletion_window_in_days = 10
}

resource "aws_s3_bucket" "bigdata-bucket-ok" {
  bucket = "bigdata-bucket-ok"
  #acl    = "private"
  acl    = "public-read-write"
  force_destroy = true

#  server_side_encryption_configuration {
#    rule {
#      apply_server_side_encryption_by_default {
#        kms_master_key_id = aws_kms_key.mykey.arn
#        sse_algorithm     = "aws:kms"
#      }
#    }
#  }
}

resource "aws_s3_bucket" "bigdata-bucket-in" {
  bucket = "bigdata-bucket-in"
  #acl    = "private"
  acl    = "public-read-write"
  force_destroy = true

#  server_side_encryption_configuration {
#    rule {
#      apply_server_side_encryption_by_default {
#        kms_master_key_id = aws_kms_key.mykey.arn
#        sse_algorithm     = "aws:kms"
#      }
#    }
#  }
}

resource "aws_s3_bucket_object" "data-in" {
  bucket = "bigdata-bucket-in"
  key    = "data.json"
  source = "/home/dezzer/Documents/TPs/BigData/data.json"

  # The filemd5() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the md5() function and the file() function:
  # etag = "${md5(file("path/to/file"))}"
 etag = filemd5("/home/dezzer/Documents/TPs/BigData/data.json")
}

resource "aws_s3_bucket" "bigdata-bucket-out" {
  bucket = "bigdata-bucket-out"
  #acl    = "private"
  acl    = "public-read-write"
  force_destroy = true

#  server_side_encryption_configuration {
#    rule {
#      apply_server_side_encryption_by_default {
#        kms_master_key_id = aws_kms_key.mykey.arn
#        sse_algorithm     = "aws:kms"
#      }
#    }
#  }
}